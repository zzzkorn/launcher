# Общая информация
Данный проект был разработан для последовательного запуска нескольких приложений с расширенным набором функциональности:
```
- Задать поведение приложения при возникновении непредвиденых ситуаций (завершилось с ошибкой, закрыто пользователем и.т.д).
- Задать взаимосвязи между приложениями, которые позволяют:
    - Автоматически сформировать порядок запуска приложений.
    - Задать минимальную величину задержки дочернего приложения, относительно запуска родительского, после которого будет 
    запущено дочернеее приложение.
    - Перезапускать все дочерние приложения в случае завершения работы родительского приложения.
- Возможность запуска лаунчера с разными пользовательскими профилями (аргументы запуска с разными профилями 
прописываются в кофигурационном файле).
- Возможность использования микроподстановок в конфигурационом файле лаунчера аргументами, передаными при
лапуске лаунчера.
- Логирование stdout и stderr запущенных процессов.
```
## Оглавление

1. [Аргументы запуска лаунчера](#Аргументы-запуска-лаунчера)
1. [Файл лаунчера](#Файл-лаунчера)
    1. [Пример файла лаунчера](#Пример-файла-лаунчера)
    2. [Поля файла лаунчера](#Поля-файла-лаунчера)
3. [Микроподстановки в файл лаунчера](#Микроподстановки-в-файл-лаунчера)
   


## Аргументы запуска лаунчера
| Обозначение | Описание | Тип| По умолчанию|Пример
|----:|:----:|:----:|:----:|:----------|
| -lf --launcher_file| Файл с настройками лаунчера | str | conf.yaml | -lf conf.yaml
| -ol --stdout_log| Логировать stdout запущенных процессов (иначе только stderr) | flag | None | -ol --stdout_log
| -p --profile| Профайл лаунчера | str | None | -p "debug no-connection"
| -pa --process_arguments| Аргументы запуска процессов | str | None | -pa "session_id=2461; type='message'; delay=5.5"

## Файл лаунчера

### Пример файла лаунчера
```yaml
# conf.yaml
main_app_1:
  working_directory: C:\Users\main_app_1
  env: bin
  mode:
    - closeOnStart
    - restartOnError
    - restartOnClose
  default: test.exe
  profiles:
    debug: test.exe -dm
    no_connection: test.exe --no-connection
  log_flags:
    - logToSeparateFile
    - disableStdOutLog
main_app_2:
  working_directory: C:\Users\main_app_2
  mode:
    - restartOnError
  profiles:
    debug: python main.py -dm
  log_flags:
    - enableStdOutLog
app_1_1:
  working_directory: C:\Users\app_1_1
  default: test.exe
  dependencies:
    - main_app_1'1s
app_1_2:
  working_directory: C:\Users\app_1_2
  default: python main.py
  dependencies:
    - app_1_1'3s
    - main_app_1'2.4s
    - main_app_2'3.5s
app_1_1_1:
  working_directory: C:\Users\app_1_1_1
  default: test.exe
  dependencies:
    - app_1_1'3s
    - main_app_1'1s
```
### Поля файла лаунчера
____
| Ключ | Описание 
|----:|:----------|
| working_directory| Рабочая директория приложения. 
Пример:
```yaml
working_directory: C:\Users\app_1_2
```

____
| Ключ | Описание 
|----:|:----------|
| default| Строка запуска приложения, если при запуске лаунчера нету совпадения с описанным в списке 'profiles' профайлом приложения. Если это поле не опреленно и нету совпадений 'profiles', приложение не запустится. 
Пример:
```yaml
default: python main.py -dm
```
```yaml
default: test.exe
```
____
| Ключ | Описание 
|----:|:----------|
| profiles| Профили работы приложения, определяющие строку запуска приложения. Профили запуска передаются аргуметом '--profile -p' при запуске лаунчера.
Пример:
```yaml
profiles:
    debug: test.exe -dm
    no_connection: test.exe --no-connection
```
____
| Ключ | Описание
|----:|:----------|
| mode| Режим работы приложения. 
- closeOnStart - перед запуском проверяет, если в диспетчере задач есть процесс с именем строки запуска приложения, уничтожает его.
- restartOnError - перезапускает приложение в случае если оно завершило работу с кодом отличным от 0.
- restartOnClose - перезапускает приложение в случае если оно завершило  кодом 0.

Пример:
```yaml
mode:
  - closeOnStart
  - restartOnError
  - restartOnClose
```
____
| Ключ | Описание
|----:|:----------|
| dependencies| Зависимость от приложений и минимальный таймаут запуска в секундах. Если не задан минимальный таймаут, то он равен 0.
Пример:
```yaml
#  При завершении работы app_1_1 или main_app_1 приложение будет так же закрыто или закрыто и перезапущено 
dependencies:
#  Текущее приложение запустится как минимум после 3 секунд с момента запуска app_1_1. 
  - app_1_1'3s
#  Текущее приложение запустится в порядке очереди запуска сразу после app_1_1. 
  - main_app_1
```
____
| Ключ | Описание
|----:|:----------|
| env| Из какой папки в рабочей директории будет запущена строка запуска приложения
Пример:
 ```yaml
#  text.exe будет запущен из C:\Users\main_app_2\bin\test.exe
main_app_2: 
  working_directory: C:\Users\main_app_2
  env: bin
  default: text.exe
```
____
| Ключ | Описание
|----:|:----------|
| log_flags| Флаги логирования
- logToSeparateFile - логировать вывод приложения в отдельный {app_name}.log файл
- disableStdOutLog - запрет логирования stdout приложения, вне зависимости от флага -ol --stdout_log.
- enableStdOutLog - логирование stdout приложения, вне зависимости от флага -ol --stdout_log.
Пример:
 ```yaml
log_flags:
  - logToSeparateFile
  - disableStdOutLog
```
## Микроподстановки в файл лаунчера
Запуск лаунчера:
```python main.py -pa "id=2461; mode='debug'; delay=5.5"```
```yaml
#conf.yaml
app_1_2:
  working_directory: C:\Users\app_1_2
  default: python main.py -m {mode}
app_1_1_1:
  working_directory: C:\Users\app_1_1_1
  default: test.exe -m {mode} -id {id}
  dependencies:
    - app_1_2'{delay}s
```
```yaml
#conf.yaml после микроподстановок
app_1_2:
  working_directory: C:\Users\app_1_2
  default: python main.py -m debug
app_1_1_1:
  working_directory: C:\Users\app_1_1_1
  default: test.exe -m debug -id 2461
  dependencies:
    - app_1_2'5.5s
```