import logging
import subprocess
import threading
from threading import Thread

import psutil
import win32api
from pip._vendor import chardet

from common.utils import parse_launcher_file
from common.utils import destroy_process_if_start
from common.variables import ENCODING
from decos import starting_delay
from descripts import UserProcessDescription
from log.config_separate_log import create_separate_logger

LOGGER = logging.getLogger('launcher')
LOGGER_CONDITION = threading.Condition()
REBOOT_CONDITION = threading.Condition()


class ErrThread(Thread):
    def __init__(self, user_process):
        super().__init__()
        self.user_process = user_process

    def run(self):
        for line in self.user_process.process.stderr:
            try:
                LOGGER_CONDITION.acquire()
                line_info = chardet.detect(line)
                line = line.decode(
                    line_info['encoding'],
                    'ignore',
                ).encode(ENCODING)
                line = line.decode(ENCODING, 'ignore').strip()
                if self.user_process.separate_logger:
                    self.user_process.separate_logger.error(line)
                line = f"{self.user_process.desc.name}: {line}"
                LOGGER.error(line)
            finally:
                LOGGER_CONDITION.release()


class OutThread(Thread):
    def __init__(self, user_process):
        super().__init__()
        self.user_process = user_process

    def run(self):
        for line in self.user_process.process.stdout:
            try:
                LOGGER_CONDITION.acquire()
                line_info = chardet.detect(line)
                line = line.decode(
                    line_info['encoding'],
                    'ignore',
                ).encode(ENCODING)
                line = line.decode(ENCODING, 'ignore').strip()
                if self.user_process.separate_logger:
                    self.user_process.separate_logger.info(line)
                line = f"{self.user_process.desc.name}: {line}"
                LOGGER.info(line)
            finally:
                LOGGER_CONDITION.release()


class RebootProcessThread(Thread):
    def __init__(self, process, reboot_on_error, reboot_on_close):
        super().__init__()
        self.process = process
        self.reboot_on_error = reboot_on_error
        self.reboot_on_close = reboot_on_close
        self.destroy = False

    def kill(self):
        self.destroy = True

    def run(self):
        while True:
            if self.destroy:
                break
            try:
                REBOOT_CONDITION.acquire()
                code = self.process.process.poll()
                if code is not None:
                    info_str = f"Процесс {self.process.desc.name} " \
                               f"завершил работу с кодом {code}"
                    LOGGER.warning(info_str)
                    if (self.reboot_on_error and code != 0) or \
                            (self.reboot_on_close and code == 0):
                        self.process.reboot()
                    else:
                        break
            finally:
                REBOOT_CONDITION.release()


class UserProcess:
    __slots__ = [
        'desc',
        'std_err_thread',
        'std_out_thread',
        'separate_logger',
        'process',
        'reboot_thread',
        'child',
    ]

    def __init__(self, launcher, name, description, log_stdout=True):
        self.desc = UserProcessDescription(description, name, log_stdout)
        # Логирование в отдельный файл
        self.separate_logger = {
            True: create_separate_logger(self.desc.name),
            False: None
        }[self.desc.separate_logger_flag]
        self.std_err_thread = None
        self.std_out_thread = None
        self.process = None
        self.reboot_thread = None
        self.child = []
        # Добавление процесса в лаунчер
        if not launcher.process_is_added(self.desc.name):
            launcher.add_process(self)
        # Инициализация дочерних потоков
        if 'child' in description:
            for ch_name, ch_description in description['child'].items():
                if not launcher.process_is_added(ch_name):
                    self.child.append(UserProcess(launcher, ch_name, ch_description, log_stdout))
                else:
                    self.child.append(launcher.get_process_and_check_start_level(ch_name, ch_description['start_level']))

    @starting_delay
    def start(self):
        if self.process is not None:
            self.process.kill()
        # Если процесс отображается в диспетчере задач, уничтожим его
        if self.desc.restart_flag:
            destroy_process_if_start(self.desc.start)
        # Запуск
        self.process = subprocess.Popen(self.desc.start,
                                        cwd=self.desc.working_directory,
                                        env=self.desc.env, shell=True,
                                        creationflags=subprocess.CREATE_NEW_CONSOLE,
                                        stdout=subprocess.PIPE if self.desc.log_stdout_flag else subprocess.DEVNULL,
                                        stderr=subprocess.PIPE)
        LOGGER.debug(f"Запущен процесс: {self.desc.name} {self.desc.start}")
        # Логирование покота ошибок
        self.std_err_thread = ErrThread(self)
        self.std_err_thread.start()
        # Логирование покота вывода
        if self.desc.log_stdout_flag:
            self.std_out_thread = OutThread(self)
            self.std_out_thread.start()
        # Перезапуск при вылете
        if self.desc.reboot_on_error_flag or self.desc.reboot_on_close_flag:
            self.reboot_thread = RebootProcessThread(self, self.desc.reboot_on_error_flag, self.desc.reboot_on_close_flag)
            self.reboot_thread.start()

    def reboot(self):
        self.start()
        # Уничтожение и перезапуск дочерних процессов
        for ch in self.child:
            ch.reboot()

    def kill(self):
        if self.reboot_thread:
            self.reboot_thread.kill()
        process = psutil.Process(self.process.pid)
        for proc in process.children(recursive=True):
            proc.kill()
        process.kill()
        LOGGER.debug(f'Процесс уничтожен: {self.desc.name}')


class UserLauncher:

    def __init__(self, init_file, profile=None, process_arguments=None, stdout_log=True):
        self.stdout_log = stdout_log
        self.descriptions = parse_launcher_file(init_file, profile, process_arguments)
        self.process = []

    def add_process(self, process):
        self.process.append(process)

    def get_process_and_check_start_level(self, process_name, start_level):
        temp = None
        for p in self.process:
            if p.desc.name == process_name:
                if p.desc.start_level < start_level:
                    p.desc.start_level = start_level
                temp = p
                break
        return temp

    def process_is_added(self, process_name):
        return True if process_name in [item.desc.name for item in self.process] else False

    def start(self):
        """
        Метод запускает лаунчер
        :return:
        """
        # Инициализацияпроцессов
        for name, description in self.descriptions.items():
            UserProcess(self, name, description, self.stdout_log)
        # Формирование очереди запуска
        self.process.sort(key=lambda x: x.desc.start_level)
        # Запуск процессов
        for p in self.process:
            p.start()

    def kill(self):
        for p in self.process:
            p.kill()

