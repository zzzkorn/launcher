import argparse
import logging
import sys

import win32con
import win32api

from common.types import process_arguments_type
from common.utils import check_conf_file
from common.variables import DEFAULT_LAUNCH_FILE, DEFAULT_PROFILE
from errors import ConfigFileNotExistError
from errors import ConfigFileExtensionError
from errors import FormatConfigFileError
from errors import WorkingDirectoryNotExistsError
from errors import EnvNotExistsError
from launcher import UserLauncher
from log import config_launcher_log

LOGGER = logging.getLogger('launcher')
LAUNCHER = None


def _create_arg_parser():
    """Создание парсера для аргументов командной строки"""
    parser = argparse.ArgumentParser(
        prog='main',
        description='''Лаунчер''',
        epilog=f'(c)Pavel',
    )

    parser.add_argument(
        '-lf', '--launcher_file',
        default=DEFAULT_LAUNCH_FILE,
        type=str,
        help='Файл с настройками лаунчера',
        metavar='FILE',
    )
    parser.add_argument(
        '-ol',
        '--stdout_log',
        action='store_const',
        const=True,
        help='Логировать stdout запущенных процессов (иначе только stderr)',
        metavar='STDOUT_LOG',
    )
    parser.add_argument(
        '-p',
        '--profile',
        default=DEFAULT_PROFILE,
        type=str,
        help='Профайл лаунчера',
        metavar='PROFILE',
    )
    parser.add_argument(
        '-pa',
        '--process_arguments',
        type=process_arguments_type,
        help='Аргументы запуска приложений',
        metavar='ARGS',
    )
    return parser


def _on_exit(launcher):
    LOGGER.debug('Console close')
    launcher.kill()


def on_exit(event):
    global LAUNCHER
    if event in (win32con.CTRL_BREAK_EVENT, win32con.CTRL_CLOSE_EVENT):
        try:
            LOGGER.debug('Консоль закрыта')
            LAUNCHER.kill()
            del LAUNCHER
        except Exception as e:
            LOGGER.error(f'Ошибка закрытия консоли: {e}')


def main():
    global LAUNCHER
    try:
        parser = _create_arg_parser()
        namespace = parser.parse_args(sys.argv[1:])
        # Проверка конфигурационного файла лаунчера
        launch_file = namespace.launcher_file
        check_conf_file(launch_file)

        profile = namespace.profile
        stdout_log = namespace.stdout_log
        process_arguments = namespace.process_arguments
        LAUNCHER = UserLauncher(
            launch_file,
            profile,
            process_arguments,
            stdout_log,
        )
        LAUNCHER.start()
        win32api.SetConsoleCtrlHandler(on_exit, True)

    except ConfigFileNotExistError as e:
        LOGGER.critical(e)
        exit(1)
    except ConfigFileExtensionError as e:
        LOGGER.critical(e)
        exit(1)
    except FormatConfigFileError as e:
        LOGGER.critical(e)
        exit(1)
    except WorkingDirectoryNotExistsError as e:
        LOGGER.critical(e)
        exit(1)
    except EnvNotExistsError as e:
        LOGGER.critical(e)
        exit(1)


if __name__ == '__main__':
    main()
