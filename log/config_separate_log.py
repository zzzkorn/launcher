import logging
import logging.handlers
import os

from common.variables import ENCODING

MAIN_FORMATTER = logging.Formatter('%(asctime)s %(levelname)s %(message)s')


def create_separate_logger(app_name):
    # Подготовка имени файла для логирования
    path = os.path.dirname(os.path.abspath(__file__))
    path = os.path.join(path, 'separate', f'{app_name}.log')

    file = logging.handlers.TimedRotatingFileHandler(path, encoding=ENCODING, interval=1, when='D')
    file.setFormatter(MAIN_FORMATTER)
    # Настройка логера
    logger = logging.getLogger(app_name)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(file)
    return logger

