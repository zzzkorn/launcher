import argparse
from collections import namedtuple

dependence = namedtuple('dependence', ['process', 'delay'])


def process_arguments_type(process_arguments):
    str_arg = process_arguments
    try:
        process_arguments = str(process_arguments)
    except ValueError:
        raise argparse.ArgumentTypeError("Параметр '-pa', '--process_arguments' должен быть типа str")
    process_arguments = process_arguments.split(';')
    for a in process_arguments:
        try:
            exec(a.strip())
        except Exception:
            raise argparse.ArgumentTypeError(f"Аргумент процесса '{a}' из набора аргументов '{str_arg}' "
                                             f"не прошел проверку корректности")
    return process_arguments
