import logging
import os
import re
from copy import copy

import psutil
import yaml

from common.types import dependence
from common.variables import ENCODING
from errors import ConfigFileNotExistError
from errors import ConfigFileExtensionError
from errors import FormatConfigFileError
from log import config_launcher_log

LOGGER = logging.getLogger('launcher')


def _format_process_arguments(launcher_file, format_file, process_arguments):
    format_lines = []
    for a in process_arguments:
        exec(a.strip())
    for line in launcher_file.readlines():
        try:
            format_lines.append(eval(fr'fr"""{line}"""'))
        except Exception as e:
            raise FormatConfigFileError(line, e)
    format_file.writelines(format_lines)


def _parse_dependencies(dependencies):
    parse_dep = []
    for d in dependencies:
        temp = d.split("'")
        if len(temp) < 2:
            delay = 0
        else:
            try:
                delay = float(re.sub("[^0-9.]", "", temp[1]))
            except Exception as e:
                LOGGER.warning(f'Некоректно задан параметр задержки включения дочернего процесса {d}: {e}')
                delay = 0
        parse_dep.append(dependence(process=temp[0], delay=delay))
    return parse_dep


def _parse_descriptions(descriptions, profile):
    parsed_descriptions = {}
    for name, description in descriptions.items():
        # Парсинг профайлов
        if 'profiles' not in description:
            description['start'] = description['default']
            del description['default']
        else:
            for pk, pv in description['profiles'].items():
                if pk in profile:
                    description['start'] = pv
                    if 'default' in description:
                        del description['default']
                    del description['profiles']
                    break
            else:
                if 'default' in description:
                    description['start'] = description['default']
        if 'start' in description:
            parsed_descriptions[name] = description
    return parsed_descriptions


def _get_dependencies_dict(descriptions, dependence_name=None, dependence_description=None, start_level=0, profile=None):
    # Первый проход функции
    if not dependence_name or not dependence_description:
        dependencies_dict = dict(filter(lambda v: 'dependencies' not in v[1], descriptions.items()))
        parsed_descriptions = _parse_descriptions(descriptions, profile)
        for name, description in dependencies_dict.items():
            child = _get_dependencies_dict(parsed_descriptions, name, description, start_level + 1)
            dependencies_dict[name]['start_level'] = start_level
            if child:
                dependencies_dict[name]['child'] = child
        return dependencies_dict
    # Последующие прохождения функции
    else:
        children = dict(filter(lambda v: True if 'dependencies' in v[1] and dependence_name in
                                                 [item.split("'")[0] for item in v[1]['dependencies']] else False,
                               descriptions.items()))
        if children:
            child = {}
            for name, description in children.items():
                child[name] = copy(description)
                child[name]['start_level'] = start_level
                if 'dependencies' in child[name]:
                    child[name]['dependencies'] = _parse_dependencies(child[name]['dependencies'])
                child_child = _get_dependencies_dict(descriptions, name, description, start_level + 1)
                if child_child:
                    child[name]['child'] = child_child
            return child
        else:
            return


def check_conf_file(file_path):
    if os.path.exists(file_path):
        name, extension = os.path.splitext(file_path)
        if extension != '.yaml':
            raise ConfigFileExtensionError(file_path)
    else:
        raise ConfigFileNotExistError(file_path)


def destroy_process_if_start(process_name):
    """
    Метод ищет среди запущенных процессов процесс с таким же именен и закрывает его
    :param process_name:
    :return:
    """
    for proc in psutil.process_iter():
        try:
            name = proc.name()
            if name == process_name:
                proc.kill()
                LOGGER.debug(f'Процесс закрыт: {process_name}')
                break
        except psutil.AccessDenied as e:
            LOGGER.warning(e)


def parse_launcher_file(file_path, profile, process_arguments):
    with open(file_path, 'r', encoding=ENCODING) as launcher_file:
        if process_arguments:
            # Создание врееменого файла с раскрытыми аргументами
            with open('temp.yaml', 'w', encoding=ENCODING) as format_file:
                _format_process_arguments(launcher_file, format_file, process_arguments)
            with open('temp.yaml', 'r', encoding=ENCODING) as format_file:
                description = yaml.load(format_file, Loader=yaml.SafeLoader)
        else:
            description = yaml.load(launcher_file, Loader=yaml.SafeLoader)
    dependencies_dict = _get_dependencies_dict(description, profile=profile)
    return dependencies_dict
