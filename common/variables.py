"""Константы"""
import logging


# Кодировка проекта
ENCODING = 'utf-8'
# Текущий уровень логирования
LOGGING_LEVEL = logging.DEBUG
DEFAULT_LAUNCH_FILE = 'conf.yaml'
DEFAULT_PROFILE = ''
