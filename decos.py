"""Декораторы"""
from time import time
from time import sleep

def starting_delay(start):
    app_start_time = dict()

    def wrapped(self):
        if self.desc.dependencies:
            for d in self.desc.dependencies:
                time_after_start = time() - app_start_time[d.process]
                if time_after_start < d.delay:
                    sleep(d.delay - time_after_start)
        app_start_time[self.desc.name] = time()
        start(self)
    return wrapped




