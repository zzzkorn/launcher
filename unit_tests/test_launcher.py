import unittest

from launcher import UserLauncher


class TestClass(unittest.TestCase):
    """
    Тесты лаунчера
    """

    def test_launcher_parse_profile_launcher_file(self):
        """Тест парсинга файла лаунчера с определенным профайлом запуска"""
        ul = UserLauncher('yaml/launcher_test_profile.yaml', ['debug_test_app', 'debug', 'error'])
        desc = ul.description
        self.assertEqual(desc['test_app_1']['start'], 'test.exe -debug')
        self.assertEqual(desc['test_app_2']['start'], 'test.exe')
        self.assertEqual(desc['test_app_3']['start'], 'test.exe')
        self.assertEqual(desc['test_app_4']['start'], 'python main.py -debug')

    def test_launcher_parse_mode_launcher_file(self):
        """Тест парсинга файла лаунчера с определенными модами запуска приложения"""
        ul = UserLauncher('yaml/launcher_test_mode.yaml')
        desc = ul.description
        self.assertTrue('пристартеперезапустить,еслизапущено' in desc['test_app_1']['mode'])
        self.assertTrue('перезапуститьпривылете' in desc['test_app_1']['mode'])
        self.assertTrue('неперезапускатьпривылете' in desc['test_app_1']['mode'])
        self.assertTrue('одиночныйзапускприложения' in desc['test_app_1']['mode'])


if __name__ == '__main__':
    unittest.main()
