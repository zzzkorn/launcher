import json
import os
import unittest

from common.utils import parse_launcher_file
from launcher import UserLauncher


class UtilsTestClass(unittest.TestCase):

    def test_parse_launcher_file(self):
        """Тест парсинга файла лаунчера с определенным профайлом запуска"""
        with open(os.path.join('json', 'utils_file_before_parser.json'), "r", encoding='utf-8') as f:
            ret_dict_2 = json.load(f)
        ret_dict_1 = parse_launcher_file(os.path.join('yaml', 'utils_file_for_parse.yaml'), ['debug'])
        self.assertEqual(ret_dict_2, ret_dict_1)


if __name__ == '__main__':
    unittest.main()
