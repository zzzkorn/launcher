import os

from errors import WorkingDirectoryNotExistsError
from errors import EnvNotExistsError


class UserProcessDescription:
    __slots__ = ['name', 'delay', 'start', 'start_level', 'restart_flag', 'log_stdout_flag', 'reboot_on_close_flag',
                 'reboot_on_error_flag', 'env', 'working_directory', 'separate_logger_flag', 'dependencies']

    def __init__(self, description, name, log_stdout):
        # Проврерка существования рабочей директории
        if 'working_directory' in description and \
                not os.path.exists(description['working_directory']):
            raise WorkingDirectoryNotExistsError(name, description['working_directory'])
        # Проврерка существования env
        if 'env' in 'description':
            env_path = os.path.join(description['working_directory'], description['env']) \
                if 'working_directory' in description else description['env']
            if not os.path.exists(env_path):
                raise EnvNotExistsError(name, env_path)
        # Форматирование поля мод
        if 'mode' in description and isinstance(description['mode'], list):
            description['mode'] = list(map(lambda x: x.strip().lower().replace(' ', '').replace('_', ''),
                                           description['mode']))
        # Форматирование поля log_flags
        if 'log_flags' in description and isinstance(description['log_flags'], list):
            description['log_flags'] = list(map(lambda x: x.strip().lower().replace(' ', '').replace('_', ''),
                                                description['log_flags']))
        # Заполнение полей
        self.start = description['start']
        self.start_level = description['start_level']
        self.name = name
        self.dependencies = description['dependencies'] if 'dependencies' in description else None
        self.restart_flag = True if 'mode' in description and 'closeonstart' in description['mode'] \
            else False
        self.reboot_on_close_flag = True if 'mode' in description and 'restartonclose' in description['mode'] \
            else False
        self.reboot_on_error_flag = True if 'mode' in description and 'restartonerror' in description['mode'] \
            else False
        self.delay = description['delay'] if 'delay' in description else 0
        if ('log_flags' in description) and ('enablestdoutlog' in description['log_flags']):
            self.log_stdout_flag = True
        elif ('log_flags' in description) and ('disablestdoutlog' in description['log_flags']):
            self.log_stdout_flag = False
        else:
            self.log_stdout_flag = log_stdout
        self.separate_logger_flag = True if ('log_flags' in description) and \
                                            ('logtoseparatefile' in description['log_flags']) else False
        self.env = {'PATH': description['env']} if 'env' in description else None
        self.working_directory = description['working_directory'] if 'working_directory' in description else None

