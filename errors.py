"""Ошибки"""


class ConfigFileNotExistError(Exception):
    """Исключение  - отсутствует файл конфигурации лаунчера"""
    def __init__(self, path):
        self.path = path

    def __str__(self):
        return f'Не удалось найти файл конфигурации лаунчера: {self.path}'


class ConfigFileExtensionError(Exception):
    """Исключение  - неправильное расширение конфигурационного файла лаунчера"""
    def __init__(self, path):
        self.path = path

    def __str__(self):
        return f'Файл "{self.path}" должен иметь расширение .yaml'


class FormatConfigFileError(Exception):
    """Исключение  - ошибка микроподстановки переменной в конфигурациооный файл лаунчера"""
    def __init__(self, line, exception):
        self.line = line
        self.exception = exception

    def __str__(self):
        return f'Не удалось выполнить подстановку в конфигурационный файл "{self.line.strip()}". ' \
               f'Ошибка: {self.exception}'


class WorkingDirectoryNotExistsError(Exception):
    """Исключение  - не найдена рабочая директория приложения"""
    def __init__(self, app_name, working_directory):
        self.working_directory = working_directory
        self.app_name = app_name

    def __str__(self):
        return f'Не удалось найти рабочую директорию "{self.working_directory}" приложения "{self.app_name}"'


class EnvNotExistsError(Exception):
    """Исключение  - не найдена директория env"""
    def __init__(self, app_name, env_directory):
        self.env_directory = env_directory
        self.app_name = app_name

    def __str__(self):
        return f'Не удалось найти директорию env "{self.env_directory}" приложения "{self.app_name}"'
