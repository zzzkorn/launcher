import os

from errors import WorkingDirectoryNotExistsError
from errors import EnvNotExistsError


class UserProcessMaker(type):
    def __init__(self, cls_name, bases, cls_dict):
        print(cls_dict)
        # Проврерка существования рабочей директории
        if 'working_directory' in cls_dict['description'] and \
                not os.path.exists(cls_dict['description']['working_directory']):
            raise WorkingDirectoryNotExistsError(cls_dict['name'], cls_dict['description']['working_directory'])
        # Проврерка существования env
        if 'env' in cls_dict['description']:
            env_path = os.path.join(cls_dict['description']['working_directory'], cls_dict['description']['env']) \
                if 'working_directory' in cls_dict['description'] else cls_dict['description']['env']
            if not os.path.exists(env_path):
                raise EnvNotExistsError(cls_dict['name'], env_path)
        super().__init__(cls_name, bases, cls_dict)
